# hotmesh-software

This project contains the software related to the hotmesh:
* gateway to connect the hotmesh network to an external network, such as internet or local network
* web application to monitor the mesh network
* REST API
* MQTT API
* mobile app
* etc.